# Freestyle Build



## Getting started
Freestyle Job:
This is  normally use for testing the application. It is not used in production certain such as, using it to deploy application in a production environment. This is usually done by pipeline job or multibranch jobs.

1)
In this first jenkins freestyle job, a simple freestyle jobs is run by checking the versions of Nodejs and npm in tinside as a shell command inside jenkins UI.

However, before this can be successful, Nodejs, npm mist be install on the server on which the jenkins container is running. Another option is to installed it as a plugin in the jenkins UI. The console output shows the success results of the version of Nodejs and npm installed.



2)

The next simple project is to connect/authenticate to GitLab(SCM/VSC) and again execute a simple .sh file just having the versions for npm and Nodejs. Before this can happen,  Jenkins system user have to be granted access preveledge to access. 

Jenkins system has to be granted permission to run the file also after accessing it. For this, the command chmod +x filename has to be place above the command that needs to be executed.


3)

The last simple freestyle job is to test and build artifact of the application by running the mvn package command. These artifact  are saved in nthe mounted jenkins-home directory inside the jenkins container.

Below are the images of the console output and the job types

 





```
cd existing_repo
git remote add origin https://gitlab.com/build-automation-ci-cd-with-jenkins/freestyle-build.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/build-automation-ci-cd-with-jenkins/freestyle-build/-/settings/integrations)

## Collaborate with your team

